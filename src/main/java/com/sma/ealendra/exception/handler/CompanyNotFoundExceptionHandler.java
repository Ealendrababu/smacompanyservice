 package com.sma.ealendra.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.sma.ealendra.exception.CompanyNotFoundException;

@RestControllerAdvice
public class CompanyNotFoundExceptionHandler {
	
	@ExceptionHandler(CompanyNotFoundException.class)
	public ResponseEntity<String> companyNotExceptionHandler(ClassNotFoundException e)
	{
		return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
