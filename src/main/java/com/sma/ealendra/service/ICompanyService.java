package com.sma.ealendra.service;

import java.util.List;

import com.sma.ealendra.entity.Company;

public interface ICompanyService {
	
	public Long createCompany(Company company);
	
	public List<Company> getAllCompanys();
	
	public Company getOneCompany(Long id);
	
	public void deleteCompany(Long id);
	
	public void updateCompany(Company company);
	
	

}
