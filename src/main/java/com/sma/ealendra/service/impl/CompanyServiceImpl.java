package com.sma.ealendra.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sma.ealendra.entity.Company;
import com.sma.ealendra.exception.CompanyNotFoundException;
import com.sma.ealendra.repo.CompanyRepository;
import com.sma.ealendra.service.ICompanyService;

@Service
public class CompanyServiceImpl implements ICompanyService {
	
	@Autowired
	private CompanyRepository repo;
	
	
	public Long createCompany(Company company) {
		
		return repo.save(company).getId();
	}
	
	
	public List<Company> getAllCompanys() {
		
		return repo.findAll();
	}
	
	
	public Company getOneCompany(Long id) {
		
		/*
		 * Optional<Company> opt= repo.findById(id); if(opt.isPresent())
		 * 
		 * return opt.get();
		 * 
		 * else throw new CompanyNotFoundException("Company Not Exist");
		 * 
		 * 
		 */
		return repo.findById(id).orElseThrow(()->new CompanyNotFoundException("Company Not Exist"));
	}
	
	
	public void deleteCompany(Long id) {
		//repo.deleteById(id);
		repo.delete(getOneCompany(id));
		
	}
	
	
	public void updateCompany(Company company) {
		
	        //repo.save(company);
		
		Long id=company.getId();
		if(id!=null && repo.existsById(id))
		{
			throw new CompanyNotFoundException("Company Not Found");
		}
		
		else
		{
			repo.save(company);
		}
		
		
	}
	
	
	
	
	
	
	
	
	

}
