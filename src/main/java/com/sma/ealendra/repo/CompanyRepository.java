package com.sma.ealendra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sma.ealendra.entity.Company;


@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>{

}
