package com.sma.ealendra.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="company_tab")
@DynamicUpdate	
public class Company {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="com_id_col")
	private Long id;
	
	@Column(name="com_name_col")
    private String name;
    
	@Column(name="com_cregId_col")
    private String cregId;
    
	@Column(name="com_ctype_col")
    private String ctype;
    
	@Column(name="com_parentOrg_col")
    private String parentOrg;
    
	@Column(name="com_mode_col")
    private String modeOfOperate;
    
	@Column(name="com_service_col")
    private String serviceCode;
    
 @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
 @JoinColumn(name="addr_fk_col")
 private Address addr; //Has-A

}
