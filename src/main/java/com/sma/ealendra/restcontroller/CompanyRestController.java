package com.sma.ealendra.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sma.ealendra.entity.Company;
import com.sma.ealendra.service.ICompanyService;

@RestController
@RequestMapping("/com")
public class CompanyRestController {
	
	@Autowired
	private ICompanyService service;
	
	@PostMapping("/create")
	public ResponseEntity<String> createCompany(@RequestBody Company company)
	{
		ResponseEntity<String> resp=null;
		try {
			
		Long id=service.createCompany(company);
		resp= ResponseEntity.ok("Company Created with id "+id);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return resp;
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Company>> getAllCompanys()
	{
		ResponseEntity<List<Company>> resp=null;
		try {
			
		List<Company> list=service.getAllCompanys();
		resp= ResponseEntity.ok(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return resp;
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<?> getOneCompany(@PathVariable Long id)
	{
		ResponseEntity<?> resp=null;
		
		try {
			
		Company company=service.getOneCompany(id);
		resp = new ResponseEntity<Company>(company,HttpStatus.OK);
		
		
			
		} catch (Exception e) {
			
			e.printStackTrace();
		resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
			//throw e;
			
		}
		
		return resp;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> deleteCompany(@PathVariable Long id)
	{
		ResponseEntity<String> resp=null;
		try {
			service.deleteCompany(id);
			resp=new ResponseEntity<String>("company id deleted "+id,HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
      resp=new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
			//throw e;
			
		}
		return resp;
	}
	
	@PutMapping("/modify")
	public ResponseEntity<String> updateCompany(@RequestBody Company company)
	{
		ResponseEntity<String> resp=null;
		try {
			service.updateCompany(company);
			resp = new ResponseEntity<String>("Company updated '"+company+"'",HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			//resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
			throw e;
			
		}
		
		return resp;
	}
	
}
